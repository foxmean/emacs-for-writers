#+TITLE: Emacs for Writers
#+AUTHOR: Mandar Vaze (transcribed form Jay Dixit's presentation)
#+DATE: 2016-01-31 03:02, 2016-02-09 12:18
:org: 
:emacs:
:spacemacs:
* Emacs for Writers : Part 1
** Background
I came across [[https://youtu.be/FtieBc3KptU][this video]] (I think) from [[https://sachachua.com/blog/category/geek/emacs/emacs-news/][Sacha Chua's weekly Emacs news]] 
(But I am not certain, may be it was a tweet.) 
Personally, I prefer to refer to the presentation (PPT/PDF) that goes with 
such video, assuming it contains written notes that viewer can refer to later.

But I could not find the notes/presentation that go along with this. 
Since the video contains a lot of gems, I decided to make my own notes 
(and in process help some other Emacs-noobs like myself).

Please note that there are lot of "visuals", so it is useful to watch the video. 
At least the second half hour is "[[https://www.youtube.com/watch?v=FtieBc3KptU&t=1200s][demo]]" mode, so there couldn't be any notes.

** Comment your articles
The way programmers write code comments, which are meant for themselves 
(or other developers). 
Similarly, writers also need to comment their articles (Who knew). 
These are Notes-for-self, but do not show up in the exported format. 
Trick is simple, start a line with a =#=, and the line is considered as as comment. 
Check the source org file of this article and see a comment right below this sentence.
# This is a comment, that will not appear in the blog.
Of course for multi-line comment, 
one can use well known =#+BEGIN_COMMENT= and =#+END_COMMENT= blocks.

If you want to comment out an entire subtree, 
use =C-c ;=. Such subtree is not exported.

** [[https://workflowy.com/][Workflowy]] meets [[https://www.literatureandlatte.com/scrivener/overview][Scrivener]]
Jay opens up with how he liked both these products separately, 
but wanted combination of these.

Outline to the left, contents on the right 
controlled by keyboard as much as possible without using mouse a lot.

He showed two "tricks" (I couldn't hear them properly, 
but here is how I was able to get such a functionality).

*** Split the window
Split the window to the right (assuming you started with single window). 
In [[https://www.spacemacs.org/][Spacemacs]] you can do this by =SPC w V=.

Now you have the same buffer in two windows. You edit in one windows, 
but the changes are visible in other window immediately.

That is not exactly what you (or Jay) want. 
He wanted outline in one (left) window and contents in the right. 
So we ..

*** Separate the buffers
Go the window on your left, 
and =clone-indirect-buffer-other-window=. 
Now you can see the outline in the left pane, and edit in the right pane. 
The left pane can remain "folded". It doesn't update unless you add a node.

* Emacs for Writers : Part 2

Here are various other tricks Jay talks about, in his presentation.

** Org-bullets

Makes pretty bullets in =org-mode=. =Spacemacs= already comes with [[https://github.com/sabof/org-bullets][this]] package. 
No additional work needed. 
Of course, you can customize the bullets to your liking, 
but I am pretty happy with the defaults.

** Notmuch for emails

Jay doesn't show [[Https://notmuchmail.org/][Notmuch]] but makes a comment about it. 
I looked at it, but I think my use case is a bit different. 
I would rather have something like [[http://www.mutt.org/][Mutt]] , 
than something just /searches/ and outsources sending 
and receiving to other programs or libraries.

** [[https://github.com/Malabarba/emacs-google-this][Google search]]

I was amazed to see that Emacs lets you search google from within 
(without opening up the browser).
=Spacemacs= lets you initiate a search (but opens the results in a browser). 
Try =SPC s w g=

** Abbreviations:

This is like a [[https://www.autohotkey.com/][Autohotkey]] program some of you might have used. 
As a writer, Jay uses it extensively. 
He shows his =abbrevs_defs= file, which is huge. 
Jay used abbreviation /bq/ to insert 
=#+BEGIN_QUOTE= and =+END_QUOTE= pair. 
May be it was a snippet ([[https://github.com/joaotavora/yasnippet][yasnippet]] ??).
I couldn't make out.

** nvALT

Jay says that if someone wants to move away from MS Word
(he is very unhappy with MS Word as a "writer's tool").
Hence the need to combine workflowy with scrivener (refer to [[* Emacs for Writers : Part 1][previous Part]]), 
he would recommend [[https://brettterpstra.com/projects/nvalt/][this]] program to them rather than uphill learning curve of Emacs. 
I looked at it, 
but the program doesn't seem to be updated in at least couple of years. 
Plus there is a [[https://jblevins.org/projects/deft/][Deft]] mode in Emacs, which I think is similar.

** Fountain mode

Jay shows a screen play. 
Apparently [[https://github.com/rnkn/fountain-mode][fountain-mode]] is a standard (text-based) file format, 
understood by others tools as well.

** [[http://www.bobnewell.net/publish/35years/poetry.html#org8450e52][Poetry mode]]

Counts number of syllables in the line, shows words rhyme. 
With the word under cursor. I was blown away to see the demo.

** Org-mac link grabber

I couldn't catch the details. 
This essentially does a lot of things like capture the URL from the browser, 
and inserts it into an org-mode file, with the text and URL. 
This is useful for references.

[Update: 2016-06-21 : [[http://orgmode.org/worg/org-contrib/org-mac-link.html][Here]] you go]

** Which Emacs (distribution) do you use ?
Jay settled on [[https://github.com/railwaycat/homebrew-emacsmacport][railwaycat Emacs]] after trying [[http://aquamacs.org/][Aquamacs]] It has a lot of =OSX= specific functionality like :

- Pinch to zoom
- Swipe to navigate between frames

Since Spacemacs documentation already [[https://github.com/syl20bnr/spacemacs#os-x][suggests]] this distribution, 
turns out I was already using it but didn't know the cool features.

** Setting the title of the window

Out of the box, my Emacs window has a boring title like =Emacs= 
instead of the filename (Turns out I had not noticed that.) 
Seems like there is a variable for "fix" that.

I couldn't catch the details from the video, so I [[https://twitter.com/mandarvaze/status/694253808138891264][asked]] him about it, 
and he was kind enough to [[https://twitter.com/jaydixit/status/696394667869544448][respond]].

** Buffer stack

Shows the demo of how to move thru the relevant buffers/file. 
He has configured it such that buffers like =*Messages*= are not part of the stack

** Config files in org-mode

So that they can be well documented/shared. 
This can be done via =org-babel=, although being a beginner, 
I am yet to do that myself.

** Closing remarks
I want to Thank Jay for this presentation, 
and [[https://www.youtube.com/user/ThoughtbotVideo][Thoughtbot]] for sharing this video with us.

It is inspiring to see a "non programmer" use emacs so "ably". 
As a non-programmer, he brings in a unique point of view.

It is also comforting (for a Emacs beginner like myself) 
that if a non-programmer can make so many customizations, 
solely depending on the kindness of the Emacs community at large, 
then deciding to learn Emacs after being a [[https://www.vim.org/][Vim]] user for 20 years ain't bad choice.
